
import java.util.Arrays;

public class MergeSortTestMine
{
    public static void main(String[] args) 
    {
        int nums[] = {1,2,3,-1,-2,-4,5};
        
        printArray(nums);
        
        MergeSort.concurrentMergeSort(nums);
        
        printArray(nums);
        
        int nums2[] = {1,2,3,4};
        
        printArray(Arrays.copyOfRange(nums2, 0, (nums2.length-1)/2));
    }
    
    public static void printArray(int nums[])
    {
        for(int i=0;i<nums.length;i++)
        {
            System.out.print(nums[i]+", ");
        }
        
        System.out.println("");
    }
}