import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

public class MergeSort {

    private static final Random RNG    = new Random(10982755L);
    private static final int    LENGTH = 1024000;
    private static final int THREADS = 1;

    public static void main(String... args)
    {
        //Variables
        int[] arrT11 = randomIntArray(1024000);
        int[] arrT12 = randomIntArray(2048000 );
        int[] arrT13 = randomIntArray(8192000 );
        int[] arrT21 = Arrays.copyOf(arrT11, arrT11.length);
        int[] arrT22 = Arrays.copyOf(arrT12, arrT12.length);
        int[] arrT23 = Arrays.copyOf(arrT13, arrT13.length);
        int[] arrT41 = Arrays.copyOf(arrT11, arrT11.length);
        int[] arrT42 = Arrays.copyOf(arrT12, arrT12.length);
        int[] arrT43 = Arrays.copyOf(arrT13, arrT13.length);


        System.out.println("One thread");
        long start = System.currentTimeMillis();
        concurrentMergeSort(arrT11, 1);
        long end = System.currentTimeMillis();
        System.out.println(1024000+":"+(end-start));

        start = System.currentTimeMillis();
        concurrentMergeSort(arrT12, 1);
        end = System.currentTimeMillis();
        System.out.println(2048000+":"+(end-start));

        start = System.currentTimeMillis();
        concurrentMergeSort(arrT13, 1);
        end = System.currentTimeMillis();
        System.out.println(8192000+":"+(end-start));

        System.out.println("\nTwo thread");
        start = System.currentTimeMillis();
        concurrentMergeSort(arrT21, 2);
        end = System.currentTimeMillis();
        System.out.println(1024000+":"+(end-start));

        start = System.currentTimeMillis();
        concurrentMergeSort(arrT22, 2);
        end = System.currentTimeMillis();
        System.out.println(2048000+":"+(end-start));

        start = System.currentTimeMillis();
        concurrentMergeSort(arrT23, 2);
        end = System.currentTimeMillis();
        System.out.println(8192000+":"+(end-start));

        System.out.println("\nfour thread");
        start = System.currentTimeMillis();
        concurrentMergeSort(arrT41, 4);
        end = System.currentTimeMillis();
        System.out.println(1024000+":"+(end-start));

        start = System.currentTimeMillis();
        concurrentMergeSort(arrT42, 4);
        end = System.currentTimeMillis();
        System.out.println(2048000+":"+(end-start));

        start = System.currentTimeMillis();
        concurrentMergeSort(arrT43, 4);
        end = System.currentTimeMillis();
        System.out.println(8192000+":"+(end-start));

    }

    public static int[] randomIntArray() {
        int[] arr = new int[LENGTH];
        for (int i = 0; i < arr.length; i++)
            arr[i] = RNG.nextInt(LENGTH * 10);
        return arr;
    }

    public static int[] randomIntArray(int length)
    {
        int[] arr = new int[length];
        for (int i = 0; i < arr.length; i++)
            arr[i] = RNG.nextInt(LENGTH * 10);
        return arr;
    }

    public static boolean sorted(int[] arr) 
    {
        //Given an arr we need to determine whether or not it is sorted or not
        //We assume the array is true then we loop through the array to determine
        //whether it is sorted or not
        boolean output = true;
        
        //We are checking two consecutive numbers at a time, if the number in the front
        //is smaller than the number in the back, hence is not sorted return false
        for(int i=0;i<arr.length-1;i++)
        {
            //Not sorted
            if(arr[i] > arr[i+1])
            {
                return false;
            }
        }
        
        //If we are out here that means the array is sorted hence return output
        return output;
    }

    public static void concurrentMergeSort(int[] arr)
    {
        //Passing the array that we want to sort and the number of available processors
        //to the actual method that does the sorting and making the threads
        concurrentMergeSort(arr, THREADS);
    }

    public static void concurrentMergeSort(int[] arr, int threadCount)
    {
        //This is the base case when the thread number hits 1 we will be actually
        //running the sort
        if(threadCount <= 1)
        {
            mergeSort(arr);
        }
        //If the threadCount is greater than 1, then that means we can still split
        //the threads into halves until threadCount hits 1
        else
        {
            int length = arr.length;
            
            //We only start sorting if the array have at least more than 1 element
            if(length > 1)
            {
                //This is gettign the middle index so we can start collecting left
                //and right part of the array
                //int middle = (length-1)/2;
                
                int leftArray[] = Arrays.copyOfRange(arr,0, length/2);
                int rightArray[] = Arrays.copyOfRange(arr, length/2, length);
                /*
                int leftArray[] = new int[middle+1];
                int rightArray[] = new int[length-middle-1];

                //j and k are the indices for left and right arrays
                int j = 0;
                int k = 0;
                for(int i=0;i<=middle;i++)
                {
                    //Setting the actual element
                    leftArray[j] = arr[i];

                    //Incrementing the counter
                    j++;
                }

                for(int i=middle+1;i<length;i++)
                {
                    //Setting the actual element
                    rightArray[k] = arr[i];

                    //Incrementing the counter
                    k++;
                }*/
                //Then after the two for loops we will have the left and right arrays
                //for the left and right threads

                Thread leftThread = new Thread(new Sorting(leftArray, threadCount/2));
                Thread rightThread = new Thread(new Sorting(rightArray, threadCount/2));

                //Running the threads, don't call run call start!
                leftThread.start();
                rightThread.start();

                //Now we want the leftThread and rightThread to finish first before
                //we continue to merge the two sorted arrays from the left and right thread
                //therefore we use join to do guratee that the those two threads finishes
                //before moving on to merging
                try
                {
                    leftThread.join();
                    rightThread.join();
                } 
                catch(InterruptedException ex)
                {
                    System.out.println(ex);
                }

                //Now after the two join method we are guratee that leftArray and rightArray
                //are sorted thus we merge them together
                merge(leftArray, rightArray, arr);
            }
        }
    }
    
    //This is the merge method that will be actually merging together two sorted arrays
    public static void merge(int leftArray[], int rightArray[], int original[])
    {
        //We have two array that are sorted, we need combine the left and right sorted
        //array replace all of the element in orginal
        //i will be representing the index of the original index
        int i = 0;
        
        //j and k will be representing the left and right array's indices respectively
        int j = 0;
        int k = 0;
        
        //Getting the size of the left and right array
        int leftSize = leftArray.length;
        int rightSize = rightArray.length;
        
        //We will be keep gathering the element from the two arrays and input it
        //into the original we finishes with one or both array
        while(j < leftSize && k < rightSize)
        {
            //This means that leftArray's element is less than the
            //rightArray's element hence we take the leftArray's element
            if(leftArray[j] <= rightArray[k])
            {
                //Setting the element
                original[i] = leftArray[j];
                
                //Incrementing both counters
                j++;
                i++;
            }
            //This means that rigthArray's element is less than the leftArray's
            //element hence we take the rightArray's element
            else if(leftArray[j] > rightArray[k])
            {
                //Setting the element
                original[i] = rightArray[k];
                
                //Incrementing both counters
                k++;
                i++;
            }
            //This is when they are equal, we will be taking the leftArray's element
            else
            {
                //Setting the element
                original[i] = leftArray[j];
                
                //Incrementing both counters
                j++;
                i++;
            }
        }
        
        //If we are out here then we must check whether or not if there are any
        //left over in either array and just finishes it off
        while(j < leftSize)
        {
            //Setting the elements
            original[i] = leftArray[j];
            
            //Increment i and j
            i++;
            j++;
        }
        
        //This picks out all the left over elements in the right array
        while(k < rightSize)
        {
            //Setting the element
            original[i] = rightArray[k];
            
            //Increment i and k
            i++;
            k++;
        }
        
        //And we are done after
    }
    
    //This is the actual mergeSorting, it divides the given array into literally
    //two different arrays and sort them recursively. Basically keep dividing until
    //there is only one element then it will merge them together
    public static void mergeSort(int arr[])
    {
        //Getting the length of array
        int length = arr.length;
        
        //This means that there is at least 2 elements in the array thus
        //we have to recursively call the mergeSort method
        if(length >= 2)
        {
            //Calling copyOfRange is way more efficient than using for loops
            int leftArray[] = Arrays.copyOfRange(arr,0, length/2);
            int rightArray[] = Arrays.copyOfRange(arr, length/2, length);
            
            //Now we have both the elements from leftArray and rightArray
            //we recursively call the mergeSort on it
            mergeSort(leftArray);
            mergeSort(rightArray);
            
            //After it we will have two sorted arrays thus we just have to call
            //the merge algorithm to merge the two sorted arrays
            merge(leftArray, rightArray, arr);
        }
    }
    
    /*
    //Normal merge sort
    public static void mergeSort(int arr[], int lower, int upper)
    {
        //This means that there is at least 2 or more elements in the array
        //therefore we need to sort it using mergeSort
        if(lower < upper)
        {
            //This is finding the middle indices for splitting the array into 2
            //about equal halves
            int middle = (lower+upper)/2;
            
            //This is recursively sorting the first half
            mergeSort(arr, lower, middle);
            
            //This is recursively sorting the second half
            mergeSort(arr, middle+1, upper);
            
            //After the first half and second half is sorted recursively
            //we call merge to merge the two sorted array together
            merge(arr, lower, middle, upper);
        }
        //That means that there is only 1 element or there is none element to sort 
        //therefore there is no need to call any recursive methods
    }
    
    
    
    //Normal merge
    //This is the helper method that will merge two sorted array together
    public static void merge(int arr[], int lower, int middle, int upper)
    {
        //First we have to figure out how many elements are in the left sorted array
        //and how many elements are in the right sorted array
        int leftSize = middle-lower+1;
        int rightSize = upper-middle;
        
        //Then we create the corresponding array
        int leftArray[] = new int[leftSize];
        int rightArray[] = new int[rightSize];
        
        //Let's make also two index counters to increment the left and right array
        int j = 0;
        int k = 0;
        
        //Then we retrive the corresponding sorted element into the leftArray and
        //the rightArray
        for(int i=lower;i<=upper;i++)
        {
            //This element belongs in the leftArray
            if(i <= middle)
            {
                //Setting the element
                leftArray[j] = arr[i];
                
                //Incrementing the counter
                j++;
            }
            //This element belongs in the rightArray
            else
            {
                //Setting the elemnt
                rightArray[k] = arr[i];
                
                //Incrementing the counter
                k++;
            }
        }
        
        //Now after the previous for loop we have the two sorted array
        //that we must merge together in place in order to sort the that part 
        //of the array
        
        //We should also reset the counter for both array
        j = 0;
        k = 0;
        
        //And make a counter for the actual array
        int counter = lower;
        
        //This while loop will replacing the actual array with the left and right
        //sorted array until it finishes with one or with both, or left one array
        //untouched
        while(j < leftSize && k < rightSize)
        {
            //We take the right array's element
            if(leftArray[j] > rightArray[k])
            {
                arr[counter] = rightArray[k];
                
                //Increment the sorted array counter
                k++;
                
                //Increment the actual array counter
                counter++;
            }
            //We take the left array's element
            else if(leftArray[j] < rightArray[k])
            {
                arr[counter] = leftArray[j];
                
                //Increment the sorted array counter
                j++;
                
                //Increment the actual array counter
                counter++;
            }
            //This means that the two sorted array are on the same elemnt
            //we take the left one
            else
            {
                arr[counter] = leftArray[j];
                
                //Increment the sorted array counter
                j++;
                
                //Increment the actual array counter
                counter++;
            }
        }
        
        //After the merging process, there might be a case where there is leftover
        //in one of the arrays, so we fill in the rest if there is any left
        //This is for the left array
        while(j < leftSize)
        {
            arr[counter] = leftArray[j];
            
            //Increment counter and j
            counter++;
            j++;
        }
        
        while(k < rightSize)
        {
            arr[counter] = rightArray[k];
            
            //Increment counter and k
            counter++;
            k++;
        }
        
        //Now after everything the array is finally sorted
        
    }
    */
}