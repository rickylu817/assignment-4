import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class WordCounter
{
    // The following are the ONLY variables we will modify for grading.
    // The rest of your code must run with no changes.
    public static final Path FOLDER_OF_TEXT_FILES = Paths.get("C:\\Users\\Rwiky\\Desktop\\TestFile"); // path to the folder where input text files are located
    public static final Path WORD_COUNT_TABLE_FILE = Paths.get("C:\\Users\\Rwiky\\Desktop\\CSE216 HW4"); // path to the output plain-text (.txt) file
    public static final int NUMBER_OF_THREADS = 1;                // max. number of threads to spawn

    public static void main(String... args)
    {
        System.out.println(Runtime.getRuntime().availableProcessors());
        try
        {
            //This is the file writer that I will be using to write the output file
            //The output file will have the name output.txt
            PrintWriter outputStream  = new PrintWriter(WORD_COUNT_TABLE_FILE.toString()+"\\output.txt");

            //This is the starting time
            long start = System.currentTimeMillis();

            //First we will be looping through all the individiual text files, and with each text file
            //we will be making a thread
            File path = new File(WordCounter.FOLDER_OF_TEXT_FILES.toString());

            //Getting the actual list of files
            File files[] = path.listFiles();
            Arrays.sort(files);

            //We will be storing all of the words counts for each map inside a
            //array of Map<String, Integer>. Each of the indicies refer to the corresponding
            //file to path.listFiles()
            final Map<String, Integer> maps[] = new HashMap[files.length];
            int mapCounter = 0;

            //This is the special case where we have to handle if the thread number is 0
            //meaning that we don't spawn any thread just 1 thread to handle all of it
            if(NUMBER_OF_THREADS <= 0)
            {
                for(File f:files)
                {
                    try
                    {
                        //Scanner will be looking through the text filesd
                        Scanner scanner = new Scanner(f);

                        //Makign the hashMap that we will have to put inside
                        //maps[mapIndex]
                        Map<String, Integer> toPut = new HashMap<>();

                        //We will keep looping through the text file until
                        //we have run out of words to count
                        while(scanner.hasNext())
                        {
                            //This gets the next available word from the text file
                            String eachWord = scanner.next();

                            //We will filter out all puncutuations, meaning that
                            //words like don't become dont
                            String filtered = eachWord.replaceAll("[^a-zA-Z]", "").toLowerCase();

                            //We have to make sure that it is not an empty string
                            //before we input it into the table
                            if(!filtered.isEmpty())
                            {
                                //This means that the map does not have this current
                                //word therefore we will just put the word in it
                                //along with the count 1
                                if(!toPut.containsKey(filtered))
                                {
                                    toPut.put(filtered, 1);
                                }
                                //This means that the map does indeed have the word
                                //already thus we will put the word in it again
                                //along with the count incrementing it by 1
                                else
                                {
                                    toPut.put(filtered, toPut.get(filtered)+1);
                                }
                            }
                        }

                        //Then if we are out here that means toPut have finished counting
                        //all of the words thus we can just put it to where it belongs
                        //in the map
                        maps[mapCounter] = toPut;
                        mapCounter++;
                    }
                    catch(FileNotFoundException ex)
                    {
                        System.out.println("The file is not found");
                    }
                }
            }
            else
            {
                //We will be looping through all of the files and with each file we will
                //start a corresponding thread if possible
                for(int i=0;i<files.length;i++)
                {
                    //This checks if there is free threads that we can use
                    if(getTotalThread()-1 < NUMBER_OF_THREADS)
                    {
                        //Getting the text file that we will be giving it to the
                        //thread
                        File workingOn = files[i];

                        //Since we cannot pass any local varaiables that we are going to change
                        //into the inner class we will make a final count int that will represent
                        //the indicies that the thread will be using for maps
                        final int count = mapCounter;

                        //Then increment the mapCounter to have the next thread use the next
                        //available Map
                        mapCounter++;

                        //This is making the actual thread to parse through the files
                        Thread threadI = new Thread(new Runnable()
                        {
                            //Taking the actual file that we are working on from the outer scope
                            File x = workingOn;

                            //This is the specific map that we will be using for this thread
                            //from the array of maps
                            int mapIndex = count;
                            public void run()
                            {
                                try
                                {
                                    //Scanner will be looking through the text filesd
                                    Scanner scanner = new Scanner(x);

                                    //Makign the hashMap that we will have to put inside
                                    //maps[mapIndex]
                                    Map<String, Integer> toPut = new HashMap<>();

                                    //We will keep looping through the text file until
                                    //we have run out of words to count
                                    while(scanner.hasNext())
                                    {
                                        //This gets the next available word from the text file
                                        String eachWord = scanner.next();

                                        //We will filter out all puncutuations, meaning that
                                        //words like don't become dont
                                        String filtered = eachWord.replaceAll("[^a-zA-Z]", "").toLowerCase();

                                        //We have to make sure that it is not an empty string
                                        //before we input it into the table
                                        if(!filtered.isEmpty())
                                        {
                                            //This means that the map does not have this current
                                            //word therefore we will just put the word in it
                                            //along with the count 1
                                            if(!toPut.containsKey(filtered))
                                            {
                                                toPut.put(filtered, 1);
                                            }
                                            //This means that the map does indeed have the word
                                            //already thus we will put the word in it again
                                            //along with the count incrementing it by 1
                                            else
                                            {
                                                toPut.put(filtered, toPut.get(filtered)+1);
                                            }
                                        }
                                    }

                                    //Then if we are out here that means toPut have finished counting
                                    //all of the words thus we can just put it to where it belongs
                                    //in the map
                                    maps[mapIndex] = toPut;
                                }
                                catch(FileNotFoundException ex)
                                {
                                    System.out.println("The file is not found");
                                }
                            }
                        });

                        //Starting the thread
                        threadI.start();
                    }
                    //We will stuck in the for loop and wait until a thread is available
                    //for us to use
                    else
                    {
                        i--;
                    }
                }
            }


            //Meaning other threads is still working
            while(getTotalThread() != 1)
            {
                //Don't do anything until all the other threads finishes
            }

            //The end time for the process
            long end = System.currentTimeMillis();

            //Now outsidde of here we have every component that we need to assemble the
            //total table. It will have every single word inputted into it with a total
            Map<String, Integer> total = new HashMap<>();

            //We have to input every single word and the count from each map into the
            //total map
            for(Map<String, Integer> map:maps)
            {
                Map<String, Integer> k = sortMap(map);
                k.keySet().stream().forEach(x -> {
                    //This means that the total map doesn't have the word
                    //thus put it as 1
                    if(!total.containsKey(x))
                    {
                        total.put(x, k.get(x));
                    }
                    //This means that it does so we add whats in the total map
                    //and the count that the map has
                    else
                    {
                        total.put(x, total.get(x)+k.get(x));
                    }
                });
            }

            //Then we sort the total map using the sortMap method
            Map<String, Integer> sortedTotal = sortMap(total);

            //But we have to fill in the missing words that each Map in maps don't have
            for(Map toFill:maps)
            {
                toFill = fillIn(toFill, total);
            }

            //Finding the lonest string in the words
            int longestWordLength = longestString(sortedTotal).length();

            //Find the longest file name in the files
            int longestFileLength = longestFileName(files).length();

            //The titleFormat will be holding all the format of the rest of the
            //tables, remember the first column needs to find the longest word
            //from the text and then plus one
            String titleFormat = "%-"+(longestWordLength+1)+"s";

            //This formats the rest of columns depending on the longest file name
            for(int i=0;i<=maps.length;i++)
            {
                titleFormat += "%-"+(longestFileLength+3)+"s";
            }

            //We need two extra column because of the word list column and the total
            //column
            String title[] = new String[files.length+2];

            //The words column
            title[0] = "";

            //The total column
            title[title.length-1] = "Total";

            //This is assigning the file name to the corresponding array
            for(int i=1;i<title.length-1;i++)
            {
                String rawFileName = files[i-1].getName();
                int comma = rawFileName.indexOf(".");

                //Strip the .txt extension
                String fileName = rawFileName.substring(0, comma);

                title[i] = fileName;
            }
            //Writing the first line to the text file
            outputStream.println(String.format(titleFormat, title));
            System.out.println(String.format(titleFormat,title));
            //This returns the list of total words in a sorted manner
            String totalWords[] = sortedTotal.keySet().stream().toArray(String[]::new);

            //Now we will be printing every single word along with the counts
            for(int i=0;i<sortedTotal.keySet().size();i++)
            {
                //This array represent the word and the corresponding count for each file
                String wordAndCount[] = new String[files.length+2];

                //Start gathering the words, we don't do it to the last column
                for(int j=0;j<files.length+1;j++)
                {
                    //AAdding the title to the array
                    if(j == 0)
                    {
                        wordAndCount[j] = totalWords[i];
                    }
                    //Then rest is the numbers
                    else
                    {
                        wordAndCount[j] = maps[j-1].get(totalWords[i])+"";
                    }
                }

                //The last entry is the total count
                wordAndCount[wordAndCount.length-1] = sortedTotal.get(totalWords[i])+"";

                //System.out.println(String.format(titleFormat, wordAndCount));
                //This is the formatted output that is going to be writing out to the file name
                String output = String.format(titleFormat, wordAndCount);
                System.out.println(output);
                outputStream.println(output);
            }

            //Closing the writer
            outputStream.close();

            System.out.println(end-start);
        }
        catch(IOException ex)
        {
            System.out.println("Error have occured");
        }

    }

    //This method will simplify print out the given map in a nicely fashion
    public static void printMap(Map<String, Integer> map)
    {
        map.keySet().stream().map(x -> x+" -> "+map.get(x)).forEach(System.out::println);
    }

    //This method will return a sorted map of the given map
    public static Map<String, Integer> sortMap(Map<String, Integer> map)
    {
        Map<String, Integer> output = map.entrySet().stream().sorted(Map.Entry.<String, Integer>comparingByKey()).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        return output;
    }

    //This specific method will be responsible to give in all of the words that is
    //missing in toFill from total, with the count of 0.
    //This is to make the final printing of the tabe much easier to handle
    public static Map<String, Integer> fillIn(Map<String, Integer> toFill, Map<String, Integer> total)
    {
        total.keySet().forEach(key -> {
            //This means that the toFill map doesn't have this specific word 
            //thus we fill it in and give it a 0 count
            if(!toFill.containsKey(key))
            {
                toFill.put(key, 0);
            }
            //Else we don't do anything because it have it inside already
        });

        return toFill;
    }

    //This method will find the longest Key string form the given map
    public static String longestString(Map<String, Integer> map)
    {
        //Using stream and reduce to find the longest string in a map
        String output = map.keySet().stream().reduce("", (answer, element) ->
        {
            if(answer.length() < element.length())
            {
                return element;
            }
            else
            {
                return answer;
            }
        });

        return output;
    }

    //This one will find the longest file name without the extensions
    public static String longestFileName(File files[])
    {
        String output = "";

        for(File f:files)
        {
            String rawFileName = f.getName();
            int comma = rawFileName.indexOf(".");

            String fileName = rawFileName.substring(0, comma);

            //This means that the file we currently have have a longer name than our assumption
            //thus we update our output
            if(output.length() < fileName.length())
            {
                output = fileName;
            }
        }

        return output;
    }

    //Because Intellij is so special that it spawns in an extra thread if you run it normally
    //I have to take into account the fact that it will spawn an extra thread
    //This find the total number of active thread without counting that stupid extra thread
    public static int getTotalThread()
    {
        int output = Thread.activeCount();
        Set<Thread> threads = Thread.getAllStackTraces().keySet();

        for(Thread t:threads)
        {
            if(t.getName().equals("Monitor Ctrl-Break"))
            {
                output = output-1;
            }
        }
        return output;
    }
}
