import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Rwiky
 */
public class MergeSortTest
{
    /**
     * Test of sorted method, of class MergeSort.
     */
    @Test
    public void testSorted()
    {
        //Array to test with
        int none[] = {};
        int single[] = {1};
        int two[] = {1,2};
        int three[] = {-2,3,1};
        int four[] = {1,-3,-2,1};
        int fiveSorted[] = {-1,0,1,3,5};
        int fiveNSorted[] = {-1,-2,-3,-4,-5};
        int more1[] = MergeSort.randomIntArray();
        int more2[] = {1,1,1,1,1,1};
        int more3[] = {-1,-1,-1,-1};
        int more4[] = {1,1,2,1,1,1};
        int more5[] = MergeSort.randomIntArray();
        
        //Test cases
        assertAll(
                //Boundary 1
                () -> assertTrue(MergeSort.sorted(none)),
                //Boundary 2
                () -> assertTrue(MergeSort.sorted(single)),
                //True case 1
                () -> assertTrue(MergeSort.sorted(two)),
                //False case 1
                () -> assertFalse(MergeSort.sorted(three)),
                //False case 2
                () -> assertFalse(MergeSort.sorted(four)),
                //False case 3
                () -> assertFalse(MergeSort.sorted(more1)),
                //False case 4
                () -> assertFalse(MergeSort.sorted(fiveNSorted)),
                //True case 2
                () -> assertTrue(MergeSort.sorted(fiveSorted)),
                //True case 3
                () -> assertTrue(MergeSort.sorted(more2)),
                //True case 4
                () -> assertTrue(MergeSort.sorted(more3)),
                //False case 5
                () -> assertFalse(MergeSort.sorted(more4)),
                //False case 6
                () -> assertFalse(MergeSort.sorted(more5))
        );
        
        
    }
    
}
